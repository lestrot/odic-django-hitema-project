from django import forms
from .models import Party, User


# class LoginForm(forms.Form):
#     pseudo = forms.CharField(max_length=35)
#     password = forms.PasswordInput()


# class RegisterForm(forms.Form):
#     pseudo = forms.CharField(max_length=35)
#     password = forms.PasswordInput()


# class PartyForm(forms.Form):
#     theme = forms.CharField(max_length=100)
#     creatorId = forms.IntegerField()
#     creatorName = forms.CharField(max_length=35)


class AccountForm(forms.Form):
    pseudo = forms.CharField(max_length=35)
    password = forms.CharField(max_length=30)


class PartyForm(forms.ModelForm):
    class Meta:
        model = Party
        fields = '__all__'
        # exclude = ('creatorId', 'creatorName', 'isActive')
        widgets = {'creatorId': forms.HiddenInput(), 'creatorName': forms.HiddenInput(), 'isActive': forms.HiddenInput()}
        # forms.HiddenInput()}


class RegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'
        # exclude = ('userRole')

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        sujet = cleaned_data.get('sujet')
        message = cleaned_data.get('message')

        if sujet and message:  # Est-ce que sujet et message sont valides ?
            if "pizza" in sujet and "pizza" in message:
                # raise forms.ValidationError(
                #     "Vous parlez de pizzas dans le sujet ET le message ? Non mais ho !"
                # )
                self.add_error("message",
                               "Vous parlez déjà de pizzas dans le sujet, "
                               "n'en parlez plus dans le message !"
                               )

        return cleaned_data  # N'oublions pas de renvoyer les données si tout est OK


class LoginForm(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'
        # exclude = ('password',)


class UserAccount(forms.ModelForm):
    class Meta:
        model = User
        fields = '__all__'
        # exclude = ('password',)
