from django.db import models


# Create your models here.
class User(models.Model):
    # id_user = models.IntegerField(primary_key=True)
    pseudo = models.CharField(max_length=35)
    password = models.CharField(max_length=30)
    # userRole = models.IntegerField(null=True)

    class Meta:
        verbose_name = "utilisateurs"

    def __str__(self):
        return "{0}, {1}".format(self.pseudo, self.password)


class Party(models.Model):
    # id_party = models.IntegerField(primary_key=True)
    theme = models.CharField(max_length=100)
    creatorId = models.IntegerField()
    creatorName = models.CharField(max_length=35)
    isActive = models.BooleanField(default=True)

    def __str__(self):
        return self.theme


class Drawings(models.Model):
    # id_drawing = models.IntegerField(primary_key=True)
    link = models.TextField()
    userId = models.IntegerField()
    partyId = models.IntegerField()
    isModerated = models.BooleanField(default=False)

    def __str__(self):
        return self.link


class PartyPlayers(models.Model):
    partyId = models.IntegerField()
    userId = models.IntegerField()
    isModerated = models.BooleanField(default=False)

    def __str__(self):
        return 'players'
