var canvas, ctx, curseurX, curseurY, terminer, boutonCouleur, boutonFin, inputVisuel, boutonTelechargement, taille, mouseDown = 0;
function dessiner(e, t, n, o) {
    -1 == lastX && ((lastX = t), (lastY = n)),
        (r = 0),
        (g = 0),
        (b = 0),
        (a = 255),
        (e.strokeStyle = "rgba(" + r + "," + g + "," + b + "," + a / 255 + ")"),
        (e.lineCap = "round"),
        e.beginPath(),
        e.moveTo(lastX, lastY),
        e.lineTo(t, n),
        (e.lineWidth = taille.value),
        e.stroke(),
        e.closePath(),
        (lastX = t),
        (lastY = n);
}
function sourisOn() {
    (mouseDown = 1), dessiner(ctx, curseurX, curseurY, 12);
}
function sourisOff() {
    (mouseDown = 0), (lastX = -1), (lastY = -1);
}
function sourisMove(e) {
    positionSouris(e), 1 == mouseDown && dessiner(ctx, curseurX, curseurY, 12);
}
function positionSouris(e) {
    if (!e) e = event;
    e.offsetX ? ((curseurX = e.offsetX), (curseurY = e.offsetY)) : e.layerX && ((curseurX = e.layerX), (curseurY = e.layerY));
}
function touchStart() {
    positionTouch(), dessiner(ctx, touchX, touchY, 12), event.preventDefault();
}
function touchEnd() {
    (lastX = -1), (lastY = -1);
}
function touchMove(e) {
    positionTouch(e), dessiner(ctx, touchX, touchY, 12), event.preventDefault();
}
function positionTouch(e) {
    if (!e) e = event;
    if (e.touches && 1 == e.touches.length) {
        var t = e.touches[0];
        (touchX = t.pageX - t.target.offsetLeft), (touchY = t.pageY - t.target.offsetTop);
    }
}
function swapBouton() {
    boutonCouleur.classList.add("noControl"), boutonFin.classList.add("choixFin");
    var e = canvas.toDataURL("image/png");
    inputVisuel.value = e;
}
function confirmerFin() {
    if (!confirm("Vous avez terminé?")) return !1;
    swapBouton();
}
(terminer = document.querySelector(".finDessin")),
    (boutonCouleur = document.querySelector(".control")),
    (boutonFin = document.querySelector(".btnFin")),
    (canvas = document.getElementById("canvas")),
    (inputVisuel = document.getElementById("canvasVersServeur")),
    (boutonTelechargement = document.getElementById("btnTelecharger")),
    (taille = document.getElementById("taille")),
    (lastX = -1),
    (lastY = -1),
    canvas.getContext && (ctx = canvas.getContext("2d")),
    (canvas.width = window.innerWidth - 60),
    (canvas.height = 0.6 * window.innerHeight),
    (ctx.fillStyle = "white"),
    ctx.fillRect(0, 0, canvas.width, canvas.height),
    ctx && (canvas.addEventListener("touchstart", touchStart, !1),
        canvas.addEventListener("touchmove", touchMove, !1),
        canvas.addEventListener("touchend", touchEnd, !1),
        canvas.addEventListener("mousedown", sourisOn, !1),
        canvas.addEventListener("mousemove", sourisMove, !1),
        window.addEventListener("mouseup", sourisOff, !1)),
    (boutonTelechargement.onclick = function () {
        (boutonTelechargement.download = "Illustration.jpeg"),
            (allan = document.getElementById("btnTelecharger").href = document
                .getElementById("canvas")
                .toDataURL("image/jpeg", 1)
                .replace(/^data:image\/[^;]/, "data:application/octet-stream"));
    }),
    (terminer.onclick = confirmerFin);
