# Generated by Django 2.1.5 on 2020-06-10 04:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('drawing', '0004_auto_20200610_0605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='party',
            name='creatorId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='drawing.User'),
        ),
    ]
